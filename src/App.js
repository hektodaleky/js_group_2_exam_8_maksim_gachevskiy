import React, {Component, Fragment} from "react";
import "./App.css";
import {Route, Switch} from "react-router-dom";
import Main from "./containers/Main/Main";
import NavLink from "react-router-dom/es/NavLink";
import NewQuote from "./containers/NewQuote/NewQuote";

class App extends Component {
    render() {
        return (

            <Fragment>
                <div className="menu"><h6>Quotes Central</h6>
                    <p><NavLink  to="add-quote" exact>Submit new quote</NavLink></p>
                    <p><NavLink  to="/" exact>Quotes</NavLink></p></div>


                <Switch>


                    <Route path='/add-quote' exact component={NewQuote}/>
                    <Route path='/edit-quote' exact component={NewQuote}/>
                    <Route path='/' exac component={Main}/>

                </Switch>
            </Fragment>
        );
    }
}

export default App;
