import React from "react";
import "./Item.css";
const Item = props => {
    return (
        <div className="mainItem">
            <div className="countFirstItem"><p className="textItem">{props.text}</p>
                <p className="authorItem">{`—${props.author}`}</p>
            </div>
            <div className="contItem">

                <p className="editItem" onClick={props.edit}>Edit</p>
                <p className="removeItem" onClick={props.remove}>X</p>
            </div>


        </div>
    )
};
export default Item;