import React, {Component} from "react";
import "./NewQuote.css";
import axios from "axios";
class NewQuote extends Component {


    state = {
        quote: {
            categoryOption: "Motivation",
            author: "",
            textVal: ""
        },
        id: ""
    };

    oneQuote = {};

    componentDidMount() {
        const query = new URLSearchParams(this.props.location.search);
        let checkParam;


        for (let param of query.entries()) {
            checkParam = param;

            if (param[0] === "key") {

                this.setState({id: param[1]});
            }
            else
                this.oneQuote[param[0]] = param[1];
        }

        if (checkParam)
            this.setState({
                quote: this.oneQuote
            })


    }


    sendData = event => {
        event.preventDefault();
        console.log(this.state);
        axios.post('quotes.json', this.state.quote).then(response => {
            this.props.history.push({
                pathname: '/'
            });


        });

    };

    editData = (event, id) => {
        event.preventDefault();


        axios.put(`/quotes/${this.state.id}.json`, this.state.quote
        ).then(() => {
            this.props.history.push({
                pathname: '/'
            });
        });
        console.log(this.state.quote, this.state.id)


    };


    optionChanger = event => {
        let one = {...this.state.quote};
        one.categoryOption = event.target.value;
        this.setState({quote: one})
    };
    changeAuthor = (event) => {
        let one = {...this.state.quote};
        one.author = event.target.value;
        this.setState({quote: one})
    };
    textChanger = event => {
        let one = {...this.state.quote};
        one.textVal = event.target.value;
        this.setState({quote: one})
    };

    render() {
        let func;
        if (this.state.id !== "")
            func = (event) => {
                this.editData(event, this.state.id)
            };
        else
            func = this.sendData;

        return (
            <form className="newForm">
                <h3 className="newTitle">Submit new quote</h3>
                <label htmlFor="newType">Category</label>
                <select onChange={
                    this.optionChanger} id="newType" value={this.state.quote.categoryOption}>
                    <option value="Star Wars">Star Wars</option>
                    <option value="Famous People">Famous People</option>
                    <option value="Saying">Saying</option>
                    <option value="Humor">Humor</option>
                    <option value="Motivation">Motivation</option>
                </select>
                <label htmlFor="author">Author</label>
                <input id="author" type="text" onChange={
                    this.changeAuthor} value={this.state.quote.author}/>
                <label htmlFor="text">Quote text</label>
                <textarea id="text" onChange={this.textChanger} value={this.state.quote.textVal}/>
                <button onClick={
                    func} className="newButton">Save
                </button>
            </form>)
    };

}

export default NewQuote;