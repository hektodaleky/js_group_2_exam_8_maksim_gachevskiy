import React, {Component, Fragment} from "react";
import axios from "axios";
import Item from "../../components/Item/Item";
import "./Main.css";
import NavLink from "react-router-dom/es/NavLink";
import NewQuote from "../NewQuote/NewQuote";
class Main extends Component {
    state = {
        list: [],
        filter: ""
    };
    loadItems = (url) => {
        axios.get(url).then(response => {
            const list = [];
            console.log(response.data);

            for (let key in response.data) {
                list.push({...response.data[key], key})
            }
            this.setState({list});
        })
    };

    componentDidMount() {

        this.loadItems('/quotes.json');



    }

    removeTask = (id) => {
        axios.delete(`/quotes/${id}.json`).then(() => {
            console.log("Deleted!")
            this.loadItems('/quotes.json');
        });
    };


    componentDidUpdate() {
        if (this.state.filter !== window.location.pathname.replace('/', ''))
            this.setState({filter: window.location.pathname.replace('/', '')});
    }

    editComponent = (item) => {
        const queryParams = [];
        for (let i in item) {
            queryParams.push(
                encodeURIComponent(i) + '=' + encodeURIComponent(item[i])
            );
        }

        const queryString = queryParams.join('&');

        this.props.history.push({
            pathname: '/edit-quote',
            search: '?' + queryString
        });
    };

    render() {
        return (
            <Fragment>
                <ul className="navigation">
                    <li onClick={ this.changeFilter}><NavLink className="nav" to="all" exact>All</NavLink></li>
                    <li onClick={ this.changeFilter}><NavLink className="nav" to="starwars" exact>Star Wars</NavLink>
                    </li>
                    <li onClick={ this.changeFilter}><NavLink className="nav" to="famouspeople" exact>Famous
                        People</NavLink>
                    </li>
                    <li onClick={ this.changeFilter}><NavLink className="nav" to="saying" exact>Saying</NavLink></li>
                    <li onClick={ this.changeFilter}><NavLink className="nav" to="humor" exact>Humor</NavLink>
                    </li>
                    <li onClick={ this.changeFilter}><NavLink className="nav" to="motivation"
                                                              exact>Motivational</NavLink></li>
                </ul>
                <div className="mainWindow">
                    {this.state.list.map(item => {
                        if (this.state.filter === "all" || this.state.filter === ""
                            || this.state.filter === item.categoryOption.toLowerCase().replace(' ', ''))
                            return <Item text={item.textVal} author={item.author} key={item.key}
                                         remove={() => this.removeTask(item.key) } edit={() => {
                                this.editComponent(item)
                            }}/>

                    })}

                </div>
            </Fragment>)
    }
}
export default Main;